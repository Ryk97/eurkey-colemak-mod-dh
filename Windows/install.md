# EurKEY Colemak Mod-DH (Windows)

## How to install

1. Download the Microsoft Keyboard Layout Creator (MSKLC)
2. Import in MSKLC the right klc-file according to your keyboard layout:

| [EurKEY Colemak-DH ANSI](Windows/https://gitlab.com/jungganz/eurkey-colemak-mod-dh/-/raw/master/Windows/EurKEY-Colemak-DH-ANSI.klc?inline=false) | [EurKEY Colemak-DH ISO](https://gitlab.com/jungganz/eurkey-colemak-mod-dh/-/raw/master/Windows/EurKEY-Colemak-DH-ISO.klc?inline=false) |
| ------ | ------ |
3. Generate an exe-file
4. Execute this file
5. Consider to change your login password to numbers for the next login
6. Restart your pc and you are ready to go with your new layout