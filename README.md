# EurKEY Colemak-DH

This is the actual place of Colemak Mod-DH Variant of EurKEY

## Why?

To have a keyboard layout which unites the best of both worlds, an ergonomic layout, with perfect multilanguage support 
- If you search for opinions of people who tried alternative english Keyboard layouts. The big majority concludes that Colemak with the Mod-DH Variant is the best of them. Main reasons are ergonomics, typing speed. Besides, it is relatively easy to learn[¹](https://colemakmods.github.io/mod-dh/learn.html) [²](https://colemak.com/Easy_to_learn)
- Colemak has already multilanguage support, but out there are better multilanguage capabilities like the one of EurKEY. [Here](https://forum.colemak.com/topic/2623-colemakdh-eurkey/#p23454) at the second post ongoing, you find a discussion about the sense of EurKEY Colmeak-DH.

## For whom?

Europeans¹, Coders², Translators with efficiency and ergonomics needs.

¹Europeans who write a lot in English will profit the most of the ergonomics
²Based on the US-American layout and a layer for rapid navigation like in VIM

## Big Thanks to

- Steffen Brüntjen and his [EurKEY layout](https://eurkey.steffen.bruentjen.eu/) (QWERTY)
- Shai Coleman for the [Colemak layout](https://colemak.com/)
- Steve P and Contributers for the [Mod-DH of Colemak](https://colemakmods.github.io/mod-dh/)

## Layout

| Layout Version 1.3 |
|------------|
| ANSI-Layout |
| <img src="Images/eurkey-Colemak-DH-ANSI.png"> |
| ISO-Layout |
| <img src="Images/eurkey-Colemak-DH-ISO.png"> |
| Extra layers of the EurKey Colemak Mod-DH |
| <img src="Images/eurkey-Colemak-DH-extra-layers.png"> |

## Installation

| [Windows guide](/Windows/install.md) | [Linux guide](/Linux/install.md) |
|------------|------------|

## Learning the Layout

- [Link to learning recommendations on Colemak](https://colemak.com/Learn) (which can be applied almost completely to EurKEY Colemak Mod-DH)
    - **Hint:** the proper layout in the [Colemak Academy](https://www.colemak.academy/) would actually be "**Colemak-DHm**" and not "**Colemak-DH**" (20-11-14)
- Printable PDF versions of the whole layout:

| [ANSI Layout PDF](/Images/eurkey-Colemak-DH-print-ANSI.pdf) | [ISO Layout PDF](/Images/eurkey-Colemak-DH-print-ISO.pdf) |
|------------|------------|


## Todo

- [x] Creating the ISO Layout!
- [ ] Offering a Mac Version
- [x] Offering a Windows Version
- [ ] Generating an exe-file for Windows
- [x] Writing an installion guide (Linux, Windows)
- [x] Change `h j k l u n i m` directions to `m n e i u k y h` in the `AltGr+¬` extra layer if possible
- [x] Switching to the standardized Version of Colemak DH
- [ ] Implement the AltGr+¬ and the AltGr+Shift+α layer in a more elegant way (Linux)